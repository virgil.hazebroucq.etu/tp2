import data from './data.js';
import Component from './components/Component.js';
import PizzaList from './pages/PizzaList.js';
import Router from './Router.js';

const title = new Component('h1', null, ['La', ' ', 'carte']);
document.querySelector('.pageTitle').innerHTML = title.render();

const pizzaList = new PizzaList(data);
document.querySelector('.pageContent').innerHTML = pizzaList.render();

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/');
// affiche 'La carte' dans .pageTitle
// et la pizzaList dans .pageContent
