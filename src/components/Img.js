import Component from './Component';

export default class Img extends Component {
	/**
	 *
	 * @param {String} url
	 */
	constructor(url) {
		super('img', { name: 'src', value: url }, null);
	}
}
