export default class Component {
	tagName;
	children;
	attribute;

	/**
	 *
	 * @param {String} tagName
	 * @param {Object} attribute
	 * @param {String} attribute.name
	 * @param {String} attribute.value
	 * @param {String | String[]} children
	 */
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}

	render() {
		let res = `<${this.tagName} `;
		if (this.attribute) res += this.renderAttribute();
		if (this.children) {
			res += this.renderChildren();
		} else res += '/>';
		return res;
	}

	renderAttribute() {
		let res = '';
		if (this.attribute)
			res += `${this.attribute.name}="${this.attribute.value}"`;
		return res;
	}

	renderChildren() {
		let res = '>';
		if (this.children instanceof Array) {
			for (const child of this.children) {
				res += child instanceof Component ? child.render() : child;
			}
		} else {
			res += this.children;
		}
		res += `</${this.tagName}>`;
		return res;
	}
}
