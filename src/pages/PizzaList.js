import Component from '../components/Component.js';
import PizzaThumbnail from '../components/PizzaThumbnail.js';

export default class PizzaList extends Component {
	/**
	 *
	 * @param {Array} data
	 */
	constructor(data) {
		super(
			'section',
			{ name: 'class', value: 'pizzaList' },
			data.map(e => new PizzaThumbnail(e))
		);
	}
}
